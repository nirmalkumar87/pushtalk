const express = require('express');
const app = express();
const path = require('path');
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const port = process.env.PORT || 3000

server.listen(port, () => {
    console.log('Server listening at port %d', port);
  });


  // load static front end site
app.use(express.static(path.join(__dirname, 'public')));

// socket.io-server

var numUsers = 0;

io.on('connection', (socket) => {

    socket.on('radio', function(blob) {
        // can choose to broadcast it to whoever you want
        socket.broadcast.emit('voice', blob);
    });

    socket.on('talking', function(istalking) {
        // can choose to broadcast it to whoever you want
        socket.broadcast.emit('usertalking', istalking);
    });
    
});