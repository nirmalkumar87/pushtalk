

var constraints = { audio: true };
var socket = io();
var start = true;

document.getElementById("talking").style.display = "none"
document.getElementById('myBtn').addEventListener('pointerdown', mouseDown, false);
document.getElementById('myBtn').addEventListener('pointerup', mouseUp, false);


navigator.mediaDevices.getUserMedia(constraints).then(function (mediaStream) {

    mediaRecorder = new MediaRecorder(mediaStream);
    mediaRecorder.onstart = function (e) {
        this.chunks = [];
        start = false;
        console.log('Media recorder Started');
        //console.log(mediaStream.getAudioTracks());
    };

    //mediaStream.get
    mediaRecorder.ondataavailable = function (e) {
        this.chunks.push(e.data);
        console.log('data receiving');
    };

    mediaRecorder.onstop = function (e) {
        var blob = new Blob(this.chunks, { 'type': 'audio/ogg; codecs=opus' });
        socket.emit('radio', blob);
        start = true;
        console.log('Media recorder Stopped');
        //mediaRecorder.start(2000);
    };
});

// function f1() {
//     if (start) {
//         mediaRecorder.start(2000);
//         document.getElementById("myBtn").textContent = "Stop Recording";
//         socket.emit('talking', true);
//     }
//     else {
//         mediaRecorder.stop();
//         document.getElementById("myBtn").textContent = "Record";
//         socket.emit('talking', false);
//     }
// }

function mouseDown() {
    mediaRecorder.start(2000);
    evt.preventDefault();
    document.getElementById("myBtn").textContent = "Release to Stop Recording";
    socket.emit('talking', true);
    document.getElementById("myBtn").style.color = "red";
}

function mouseUp() {
    mediaRecorder.stop();
    document.getElementById("myBtn").textContent = "Press and Hold to Talk";
    socket.emit('talking', false);
    document.getElementById("myBtn").style.color = "white";
}

// When the client receives a voice message it will play the sound
socket.on('voice', function (arrayBuffer) {
    var blob = new Blob([arrayBuffer], { 'type': 'audio/ogg; codecs=opus' });
    console.log(blob);
    var audio = document.createElement('audio');
    var url = window.URL.createObjectURL(blob);
    audio.src = url;
    audio.play();
});

socket.on('usertalking', function (istalking) {
    document.getElementById("myBtn").disabled = istalking;
    if(!istalking) document.getElementById("talking").style.display = "none"; else document.getElementById("talking").style.display = "block";
});